package ua.com.skywell.kotlinweather.core.network

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ua.com.skywell.kotlinweather.BuildConfig
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * <p>For instantiating network-related objects.</p>
 *
 * @author chkv
 * @version 1
 * @since 26.05.2017
 */
@Module
class NetworkModule {

    @Provides @Singleton
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides @Singleton
    fun providesStethoInterceptor(): StethoInterceptor {
        return StethoInterceptor()
    }

    @Provides @Singleton
    fun providesOkHttpClient(loggingInterceptor: HttpLoggingInterceptor,
                             stethoInterceptor: StethoInterceptor): OkHttpClient {
        val okHttp = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            okHttp.addInterceptor(loggingInterceptor)
            okHttp.addInterceptor(stethoInterceptor)
        }

        okHttp.readTimeout(60, TimeUnit.SECONDS)

        return okHttp.build()
    }

    @Provides @Singleton
    fun providesGson(): Gson {
        return GsonBuilder()
                .serializeNulls()
                .create()
    }

    @Provides @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
    }

    @Provides @Singleton
    fun providesWeatherApi(retrofit: Retrofit): WeatherApi {
        return retrofit.create(WeatherApi::class.java)
    }

}