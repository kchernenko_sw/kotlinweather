package ua.com.skywell.kotlinweather.injection.modules

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import ua.com.skywell.kotlinweather.core.MainRepository
import ua.com.skywell.kotlinweather.core.MainRepositoryImpl
import ua.com.skywell.kotlinweather.core.network.WeatherApi
import ua.com.skywell.kotlinweather.core.storage.DbHelper
import ua.com.skywell.kotlinweather.core.storage.DbHelperImpl
import ua.com.skywell.kotlinweather.core.storage.DbOpenHelper
import ua.com.skywell.kotlinweather.injection.ApplicationContext
import javax.inject.Singleton

/**
 * <p>Module for instantiation application-related objects.</p>
 *
 * @author chkv
 * @version 1
 * @since 16.05.2017
 */
@Module
class ApplicationModule(val application: Application) {

    @Provides
    @Singleton
    internal fun provideApplication(): Application {
        return application
    }

    @Provides
    @Singleton
    @ApplicationContext
    internal fun provideContext(): Context {
        return application
    }

    @Provides
    fun providesDbHelper(dbOpenHelper: DbOpenHelper): DbHelper {
        return DbHelperImpl(dbOpenHelper)
    }

    @Provides
    fun providesMainRepository(dbHelper: DbHelper, weatherApi: WeatherApi, gson: Gson): MainRepository {
        return MainRepositoryImpl(dbHelper, weatherApi, gson)
    }
}