package ua.com.skywell.kotlinweather.injection.modules

import android.app.Activity
import dagger.Module
import dagger.Provides
import ua.com.skywell.kotlinweather.injection.ActivityScope

/**
 * <p>Module for instantiation activity-related objects.</p>
 *
 * @author chkv
 * @version 1
 * @since 16.05.2017
 */
@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    @ActivityScope
    internal fun provideActivity(): Activity {
        return activity
    }
}