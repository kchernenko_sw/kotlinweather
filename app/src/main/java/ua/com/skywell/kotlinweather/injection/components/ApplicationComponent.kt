package ua.com.skywell.kotlinweather.injection.components

import android.content.Context
import dagger.Component
import ua.com.skywell.kotlinweather.core.network.NetworkModule
import ua.com.skywell.kotlinweather.injection.ApplicationContext
import ua.com.skywell.kotlinweather.injection.modules.ActivityModule
import ua.com.skywell.kotlinweather.injection.modules.ApplicationModule
import javax.inject.Singleton

/**
 * <p>Component that is responsible for providing application-related objects.</p>
 *
 * @author chkv
 * @version 1
 * @since 16.05.2017
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetworkModule::class))
interface ApplicationComponent {

    fun addActivityComponent(activityModule: ActivityModule): ActivityComponent

    @ApplicationContext fun context(): Context

}