package ua.com.skywell.kotlinweather.ui.main

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import ua.com.skywell.kotlinweather.R
import ua.com.skywell.kotlinweather.ui.base.BaseActivity
import javax.inject.Inject
import kotlinx.android.synthetic.main.main_activity.*
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity
import ua.com.skywell.kotlinweather.ui.adapters.WeatherAdapter
import ua.com.skywell.kotlinweather.ui.adapters.WeatherItemClickListener
import ua.com.skywell.kotlinweather.utils.simpleToast

/**
 * <p>MainInfo screen.</p>
 *
 * @author chkv
 * @version 1
 * @since 12.05.2017
 */
class MainActivity: BaseActivity(), MainContract.View {

    @Inject lateinit var presenter: MainPresenter

    private var weatherAdapter: WeatherAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        addActivityComponent().inject(this)

        initRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
        presenter.getWeatherData()
        initAddCityListener()
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView()
    }

    override fun showLoading(loader: Boolean) {

    }

    override fun showWeather(weather: List<WeatherWithCity>) {
        weatherList.layoutManager = LinearLayoutManager(this)
        weatherAdapter?.setWeather(weather)
    }

    override fun showError() {
        simpleToast(getString(R.string.unknown_error_msg))
    }

    override fun showError(errorMsg: String) {
        simpleToast(errorMsg)
    }

    fun initRecyclerView() {
        weatherAdapter = WeatherAdapter()
        weatherList.adapter = weatherAdapter
        weatherList.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        weatherAdapter?.setItemClickListener(itemClick = object: WeatherItemClickListener {
            override fun onItemClicked(position: Int) {
                weatherAdapter?.getWeatherIdByPosition(position)?.let { presenter.deleteWeather(it) }
            }
        })
    }

    fun initAddCityListener() {
        addCityBtn.setOnClickListener { presenter.fetchWeatherData(city.text.toString()) }
    }
}